# gr-cdc

GNU Radio out-of-tree (OOT) module for ELEN90089\_2021\_SM1

This module contains code for running the Dynamic Spectrum Access scenario
for the CDC design project.

## Installation

To install, first clone the gr-cdc repository.

```
$ git clone https://gitlab.eng.unimelb.edu.au/elen90089-cdc/gr-cdc.git
$ cd gr-cdc/
```

Then build and install the module.

```
$ mkdir build
$ cd build/
$ cmake ..
$ make
$ sudo make install
```
You should see a number of blocks installed in GRC under the **\[cdc\]** 
category.

If you are using the old Ubuntu VM (currently installed on EDS 12 computers),
you also need to set the following environment variables in the terminal in
order to access the installed gr-cdc module.

```
$ export PYTHONPATH=/usr/local/lib/python3/dist-packages/
$ export LD_LIBRARY_PATH=/usr/local/lib/x86_64-linux-gnu/
$ sudo ldconfig
```

Add these updates to your bash profile ('~/.bashrc') and you will not need to
set these variables everytime you open a new terminal.

## Primary User Reference Code

GRC applications for running the OFDM-based primary user Tx and Rx can be found
 in the `./examples/` directory:

- `pu_tx_1_channel.grc`
- `pu_tx_4_channel.grc`
- `pu_rx_1_channel.grc`
- `pu_rx_4_channel.grc`

The central DSA database code has not yet been integrated into the PU Tx/Rx
applications.

## DSA Database

Example usage of interfacing with the DSA database code is also available in
the `./examples/` directory.

- `test_dsa_database.grc`

If you want to test out this code, first ensure you start an instance of the
database on the local machine.

```
$ ./run_dsa_database.py
```
You should then be able to run the GRC application and see KPI statistics
printed to the terminal.

The key block that you will need to integrate into your secondary link is
**DSA DB Connect**. This block should be used at both secondary user Tx and Rx.
Requests to the DSA database can then be made by passing commands to
 *DSA DB Connect* block as
[GNU Radio Messages](https://wiki.gnuradio.org/index.php/Message_Passing).
Example message formation for various command is shown in the following.

```
# get packet at Tx
msg = pmt.cons(pmt.intern('get_pkt'), pmt.PMT_NIL)

# put packet
payload = pmt.init_u8vector(64, [ii for ii in range(64)])
msg = pmt.cons(pmt.intern('put_pkt), payload)

# get current KPI report
msg = pmt.cons(pmt.intern('get_kpi'), pmt.PMT_NIL)

# get current PU mode (development only)
msg = pmt.cons(pmt.intern('get_mode'), pmt.PMT_NIL)
```
Responses from the server will be provided as messages on either the **info** or
**pkt** output message ports.
