INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_CDC cdc)

FIND_PATH(
    CDC_INCLUDE_DIRS
    NAMES cdc/api.h
    HINTS $ENV{CDC_DIR}/include
        ${PC_CDC_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    CDC_LIBRARIES
    NAMES gnuradio-cdc
    HINTS $ENV{CDC_DIR}/lib
        ${PC_CDC_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/cdcTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(CDC DEFAULT_MSG CDC_LIBRARIES CDC_INCLUDE_DIRS)
MARK_AS_ADVANCED(CDC_LIBRARIES CDC_INCLUDE_DIRS)
