/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_DB_CONNECT_IMPL_H
#define INCLUDED_CDC_DSA_DB_CONNECT_IMPL_H

#include <cdc/dsa_db_connect.h>
#include <cdc/dsa_db_interface.h>

namespace gr {
namespace cdc {

class dsa_db_connect_impl : public dsa_db_connect
{
private:
    int d_pktsize;
    dsa_db_interface d_iface;

    void handle_cmd(pmt::pmt_t msg);

public:
    dsa_db_connect_impl(std::string host,
                        int port,
                        int pktsize,
                        dsa::radio_type radio);
    ~dsa_db_connect_impl();

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items);
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_DB_CONNECT_IMPL_H */

