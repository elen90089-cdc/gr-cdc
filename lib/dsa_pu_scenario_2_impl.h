/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_PU_SCENARIO_2_IMPL_H
#define INCLUDED_CDC_DSA_PU_SCENARIO_2_IMPL_H

#include <cdc/dsa_pu_scenario_2.h>
#include <random>

namespace gr {
namespace cdc {

class dsa_pu_scenario_2_impl : public dsa_pu_scenario_2
{
private:
    int d_scenario;
    bool d_random;
    int d_samp_rate;
    float d_duration_ms;

    std::default_random_engine d_engine;
    std::uniform_int_distribution<int>* d_uniform = nullptr;
    std::vector<bool> d_active;
    int d_samp_cnt = 0;

    void update_scenario(int n_chan);

public:
    dsa_pu_scenario_2_impl(int scenario,
                           bool random,
                           int seed,
                           int samp_rate,
                           float duration_ms);
    ~dsa_pu_scenario_2_impl();

    void set_scenario(int scenario) { d_scenario = scenario; };
    
    int get_scenario(void) { return d_scenario; };
    
    void set_random(bool random) { d_random = random; };
    
    bool get_random(void) { return random; };
    
    void set_duration_ms(float duration_ms) { d_duration_ms = duration_ms; };
    
    float get_duration_ms(void) { return d_duration_ms; };

    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items);

};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_PU_SCENARIO_IMPL_H */

