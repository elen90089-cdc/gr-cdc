/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_STATS_PU_MODE_IMPL_H
#define INCLUDED_CDC_DSA_STATS_PU_MODE_IMPL_H

#include <cdc/dsa_stats_pu_mode.h>

namespace gr {
namespace cdc {

class dsa_stats_pu_mode_impl : public dsa_stats_pu_mode
{
private:
    boost::shared_ptr<gr::thread::thread> d_thread;
    bool d_finished;
    long d_period;

    // track stats
    long d_mode;
    int d_num_mode;
    int d_num_detect;
    int d_num_correct;
    int d_num_wrong;

    void run();

    void handle_pu(pmt::pmt_t msg);

    void handle_su(pmt::pmt_t msg);

public:
    dsa_stats_pu_mode_impl(long period);

    ~dsa_stats_pu_mode_impl();

    void reset_stats(bool reset=true);

    // Overload gr::block start/stop to start internal thread
    bool start();
    bool stop();
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_STATS_PU_MODE_IMPL_H */

