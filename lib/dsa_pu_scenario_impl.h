/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_PU_SCENARIO_IMPL_H
#define INCLUDED_CDC_DSA_PU_SCENARIO_IMPL_H

#include <cdc/dsa_pu_scenario.h>
#include <gnuradio/messages/msg_queue.h>
#include <random>

namespace gr {
namespace cdc {

class dsa_pu_scenario_impl : public dsa_pu_scenario
{
private:
    int d_scenario;
    int d_num_packets;
    int d_num_queued;
      
    gr::messages::msg_queue d_msg_queue;
    std::default_random_engine d_engine;
    std::uniform_int_distribution<int>* d_uniform;
    std::vector<bool> d_active;
    bool d_random;
    int d_pkt_len = 0;
    int d_pkt_req = 0;
    int d_pkt_cnt = 0;

    void handle_pkt(pmt::pmt_t msg);

    void update_scenario(int n_chan);

public:
    dsa_pu_scenario_impl(int scenario,
                         int num_packets,
                         int num_queued,
                         int seed);
    ~dsa_pu_scenario_impl();

    int work(int noutput_items,
             gr_vector_const_void_star &input_items,
             gr_vector_void_star &output_items);

};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_PU_SCENARIO_IMPL_H */

