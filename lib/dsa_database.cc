/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <cdc/dsa_database.h>
#include <iostream>

using std::chrono::steady_clock;
using boost::asio::ip::tcp;

namespace gr {
namespace cdc {

dsa_database::sptr dsa_database::make(std::string host,
                                      int port,
                                      bool share_mode,
                                      int mtu)
{
    return dsa_database::sptr(new dsa_database(host, port, share_mode, mtu));
}

dsa_database::dsa_database(std::string host,
                           int port,
                           bool share_mode,
                           int mtu)
  : d_host(host),
    d_port(port),
    d_share_mode(share_mode),
    d_mtu(mtu)
{
    d_radios.resize(4, false);
    d_timepoint = steady_clock::time_point::min();

    start();
}

dsa_database::~dsa_database()
{
    stop();
}

void
dsa_database::start()
{
    if (d_stop)
    {
        d_stop = false;
        d_listener = gr::thread::thread([this]{ accept_handler(); }); 
    }
}

void
dsa_database::stop()
{
    if (!d_stop) 
    {
        d_stop = true;
        d_listener.join();
    }
}

dsa_kpi_report
dsa_database::get_kpi_report()
{
    dsa_kpi_report report;

    gr::thread::scoped_lock(d_mtx_stats);

    if (d_timepoint != steady_clock::time_point::min())
    {
        auto now = steady_clock::now();
        std::chrono::duration<double> dur = (now - d_timepoint);

        double pu_thruput = 8 * d_pu_bytes / (1000 * dur.count());
        double pu_offered = 8 * d_pu_offer / (1000 * dur.count());
        double su_thruput = 8 * d_su_bytes / (1000 * dur.count());
        double su_offered = 8 * d_su_offer / (1000 * dur.count());
        report.pu_thruput_kbps = pu_thruput;
        report.pu_offered_kbps = pu_offered;
        report.su_thruput_kbps = su_thruput;
        report.su_offered_kbps = su_offered;
   }

    return report;
}

void
dsa_database::reset_stats()
{
    gr::thread::scoped_lock(d_mtx_stats);

    d_timepoint = steady_clock::now();
    d_pu_bytes = 0;
    d_pu_offer = 0;
    d_su_bytes = 0;
    d_su_offer = 0;
}

int
dsa_database::get_pu_mode()
{
    gr::thread::scoped_lock(d_mtx_stats);
    return d_pu_mode;
}

void
dsa_database::set_pu_mode(int pu_mode)
{
    gr::thread::scoped_lock(d_mtx_stats);
    d_pu_mode = pu_mode;
} 

void
dsa_database::accept_handler()
{
    boost::system::error_code ec;
    tcp::endpoint endpoint(tcp::v4(), d_port);
    tcp::acceptor acceptor(d_io_context, endpoint);
    acceptor.set_option(tcp::acceptor::reuse_address(true));

    std::vector<std::shared_ptr<gr::thread::thread>> threads(4);

    std::cout << "DSA DB: accepting connects" << std::endl;
    while (!d_stop)
    {
        auto sock = std::make_shared<tcp::socket>(d_io_context);
        acceptor.accept(*sock, ec);
        if (ec) break;

        // read connection message
        std::vector<char> buf(2);
        boost::asio::read(*sock, boost::asio::buffer(buf.data(), buf.size()), ec);

        if (!ec)
        {
            dsa::request_type req = static_cast<dsa::request_type>(buf[0]);
            dsa::radio_type radio = static_cast<dsa::radio_type>(buf[1]);

            bool in_use;
            {
                gr::thread::scoped_lock(d_mtx_thr);
                in_use = d_radios[radio];
            }

            if (!in_use && req == dsa::REQUEST_CONNECT) 
            {
                threads[radio].reset(new gr::thread::thread(
                    boost::bind(&dsa_database::request_handler, this, sock, radio)));
                std::cout << "DSA DB: new connection - radio:" << radio << std::endl;
            }
            else
            {
                sock->close();
            }
        }
    }

    for (int i = 0; i < threads.size(); i++)
        threads[i].reset();
}

void
dsa_database::request_handler(std::shared_ptr<tcp::socket> sock,
                              dsa::radio_type radio)
{
    boost::system::error_code ec;
    std::vector<char> buf(d_mtu);

    while (!d_stop && sock->is_open())
    {
        boost::asio::read(*sock, boost::asio::buffer(buf.data(), 1), ec);
        if (ec) break;

        dsa::request_type req = static_cast<dsa::request_type>(buf[0]);
        if (req == dsa::REQUEST_GET_PKT) 
        {
            boost::asio::read(*sock, boost::asio::buffer(buf.data(), 4), ec);
            if (ec) break;

            int nbytes;
            memcpy(&nbytes, buf.data(), sizeof(int));
            nbytes = ntohl(nbytes);

            // TODO: randomly generate data
            for (int i=0; i < nbytes; i++) buf[i] = static_cast<char>(i);

            boost::asio::write(*sock, boost::asio::buffer(buf.data(), nbytes));
            update_stats(radio, nbytes);
        }
        else if (req == dsa::REQUEST_PUT_PKT)
        {
            boost::asio::read(*sock, boost::asio::buffer(buf.data(), 4), ec);
            if (ec) break;

            int nbytes;
            memcpy(&nbytes, buf.data(), sizeof(int));
            nbytes = ntohl(nbytes);
            boost::asio::read(*sock, boost::asio::buffer(buf.data(), nbytes), ec);
            if (ec) break;

            // TODO: check packet validity
            update_stats(radio, nbytes);
        }
        else if (req == dsa::REQUEST_GET_KPI) 
        {
            dsa_kpi_report report = get_kpi_report();
            report.pu_thruput_kbps = htonl(report.pu_thruput_kbps);
            report.pu_offered_kbps = htonl(report.pu_offered_kbps);
            report.su_thruput_kbps = htonl(report.su_thruput_kbps);
            report.su_offered_kbps = htonl(report.su_offered_kbps);

            memcpy(buf.data(), &report, sizeof(dsa_kpi_report));
            boost::asio::write(*sock, boost::asio::buffer(buf.data(), sizeof(dsa_kpi_report)));
        }
        else if (req == dsa::REQUEST_GET_MODE)
        {
            int mode = htonl(get_pu_mode());
            memcpy(buf.data(), &mode, sizeof(int));
            boost::asio::write(*sock, boost::asio::buffer(buf.data(), sizeof(int)));
        }
        else if (req == dsa::REQUEST_PUT_MODE)
        {
            boost::asio::read(*sock, boost::asio::buffer(buf, sizeof(int)), ec);
            if (ec) break;

            int mode;
            memcpy(&mode, buf.data(), sizeof(int));
            set_pu_mode(ntohl(mode));
        }
        else if (req == dsa::REQUEST_RESET_KPI)
        {
            if(radio == dsa::RADIO_PU_TX ||
               radio == dsa::RADIO_PU_RX ||
               d_share_mode)
            {
                reset_stats();
            }
        }
        else // invalid request
        {
            break;
        }

    }

    sock->close();
    {
        gr::thread::scoped_lock(d_mtx_thread);
        d_radios[radio] = false;
    }
    std::cout << "DSA DB: disconnect - radio:" << radio << std::endl;
}

void
dsa_database::update_stats(dsa::radio_type radio, int nbytes)
{
    gr::thread::scoped_lock(d_mtx_stats);

    // start timer on first get_pkt request
    if (d_timepoint == steady_clock::time_point::min())
        d_timepoint = steady_clock::now();
    
    switch(radio)
    {
    case dsa::RADIO_PU_TX:
        d_pu_offer += nbytes;
        break;
    case dsa::RADIO_PU_RX:
        d_pu_bytes += nbytes;
        break;
    case dsa::RADIO_SU_TX:
        d_su_offer += nbytes;
        break;
    case dsa::RADIO_SU_RX:
        d_su_bytes += nbytes;
        break;
    default:
        break;
    }
}

} /* namespace cdc */
} /* namespace gr */
