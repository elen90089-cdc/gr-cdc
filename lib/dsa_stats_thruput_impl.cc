/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "dsa_stats_thruput_impl.h"

using std::chrono::steady_clock;

namespace gr {
namespace cdc {

dsa_stats_thruput::sptr
dsa_stats_thruput::make(long period,
                        float alpha)
{
    return gnuradio::get_initial_sptr
        (new dsa_stats_thruput_impl(period, alpha));
}


/*
 * The private constructor
 */
dsa_stats_thruput_impl::dsa_stats_thruput_impl(long period,
                                               float alpha)
  : gr::block("dsa_stats_thruput",
          gr::io_signature::make(0, 0, 0),
          gr::io_signature::make(0, 0, 0)),
    d_period(period),
    d_alpha(alpha)
{
    message_port_register_in(pmt::mp("pdu"));
    message_port_register_out(pmt::mp("tput"));
    message_port_register_out(pmt::mp("tput_new"));

    set_msg_handler(pmt::mp("pdu"),
                    boost::bind(&dsa_stats_thruput_impl::handle_pdu, this, _1));

    reset_stats();
}

/*
 * Our virtual destructor.
 */
dsa_stats_thruput_impl::~dsa_stats_thruput_impl()
{
}

bool
dsa_stats_thruput_impl::start()
{
    d_finished = false;

    d_thread = boost::shared_ptr<gr::thread::thread>(
        new gr::thread::thread(boost::bind(&dsa_stats_thruput_impl::run, this)));

    return block::start();
}

bool
dsa_stats_thruput_impl::stop()
{
    // Shut down the thread
    d_finished = true;
    d_thread->interrupt();
    d_thread->join();

    return block::stop();
}

void
dsa_stats_thruput_impl::run()
{
    while (!d_finished) {
        boost::this_thread::sleep(
            boost::posix_time::seconds(static_cast<long>(d_period)));
        
        if (d_finished)
            return;

        // elapsed time
        auto now = steady_clock::now();
        std::chrono::duration<double> dur0 = (now - d_start_tp);
        std::chrono::duration<double> dur1 = (now - d_last_tp);
        d_last_tp = now;

        // get latest stats
        double bytes_new = 0;
        {
            gr::thread::scoped_lock(d_mtx);

            bytes_new = d_bytes_new;
            d_bytes_new = 0;
        }
        d_bytes += bytes_new;
        double tput = 8.0*d_bytes / (1000.0*dur0.count());
        d_tput_new *= d_alpha;
        d_tput_new += (1 - d_alpha)*8.0*bytes_new / (1000.0*dur1.count());

        message_port_pub(pmt::mp("tput"), pmt::mp(tput));
        message_port_pub(pmt::mp("tput_new"), pmt::mp(d_tput_new));
    }
}

void
dsa_stats_thruput_impl::handle_pdu(pmt::pmt_t msg)
{
    d_bytes_new += pmt::blob_length(pmt::cdr(msg));
}

void
dsa_stats_thruput_impl::reset_stats(bool reset)
{
    if (reset)
    {
        gr::thread::scoped_lock(d_mtx);

        d_bytes = 0.0;
        d_bytes_new = 0.0;
        d_tput_new = 0.0;
        d_start_tp = steady_clock::now();
        d_last_tp = d_start_tp;
    }
}

} /* namespace cdc */
} /* namespace gr */

