/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_STATS_THRUPUT_IMPL_H
#define INCLUDED_CDC_DSA_STATS_THRUPUT_IMPL_H

#include <cdc/dsa_stats_thruput.h>
#include <chrono>

namespace gr {
namespace cdc {

class dsa_stats_thruput_impl : public dsa_stats_thruput
{
private:
    boost::shared_ptr<gr::thread::thread> d_thread;
    bool d_finished;
    long d_period;
    float d_alpha;

    // stat variables
    gr::thread::mutex d_mtx;
    std::chrono::time_point<std::chrono::steady_clock> d_start_tp;
    std::chrono::time_point<std::chrono::steady_clock> d_last_tp;
    double d_bytes = 0.0;
    double d_bytes_new = 0.0;
    double d_tput_new = 0.0;

    void run();

    void handle_pdu(pmt::pmt_t pdu);

public:
    dsa_stats_thruput_impl(long period,
                           float alpha);

    ~dsa_stats_thruput_impl();

    void reset_stats(bool reset=true);

    // Overload gr::block start/stop to start internal thread
    bool start();
    bool stop();

};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_STATS_THRUPUT_IMPL_H */

