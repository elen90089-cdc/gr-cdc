/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "dsa_pu_scenario_impl.h"

namespace gr {
namespace cdc {

dsa_pu_scenario::sptr
dsa_pu_scenario::make(int scenario,
                      int num_packets,
                      int num_queued,
                      int seed)
{
    return gnuradio::get_initial_sptr
      (new dsa_pu_scenario_impl(scenario, num_packets, num_queued, seed));
}


/*
 * The private constructor
 */
dsa_pu_scenario_impl::dsa_pu_scenario_impl(int scenario,
                                           int num_packets,
                                           int num_queued,
                                           int seed)
    : gr::sync_block("dsa_pu_scenario",
          gr::io_signature::make(0, 0, 0),
          gr::io_signature::make(1, -1, sizeof(gr_complex))),
      d_scenario(scenario),
      d_num_packets(num_packets),
      d_num_queued(num_queued),
      d_engine(seed),
      d_msg_queue(num_queued)
{
    // message port I/O
    message_port_register_in(pmt::mp("pkt"));
    message_port_register_out(pmt::mp("cmd"));
    set_msg_handler(pmt::mp("pkt"),
                    boost::bind(&dsa_pu_scenario_impl::handle_pkt, this, _1));

    d_random = (d_scenario < 0); 
}

/*
 * Our virtual destructor.
 */
dsa_pu_scenario_impl::~dsa_pu_scenario_impl()
{
    delete d_uniform;
}

void
dsa_pu_scenario_impl::handle_pkt(pmt::pmt_t msg)
{
    d_msg_queue.insert_tail(msg); 
    if (!d_pkt_len)
    {    
        pmt::pmt_t vect = pmt::cdr(msg);
        d_pkt_len = pmt::blob_length(vect) / sizeof(gr_complex);
    }
}

void
dsa_pu_scenario_impl::update_scenario(int n_chan)
{
    if (!d_active.size())
    {
        d_active.resize(n_chan);
            
        int n_scenarios = (0x1 << n_chan);
        d_uniform = new std::uniform_int_distribution<int>(0, n_scenarios - 1);
    }

    if (d_random && d_pkt_cnt <= 0)
    {
        d_scenario = (*d_uniform)(d_engine);
        //message_port_pub(pmt::mp("cmd"),
        //                 pmt::cons(pmt::mp("put_mode"), pmt::mp(d_scenario)));
        d_pkt_cnt = d_num_packets;
    }

    for (int i_chan=0; i_chan < n_chan; i_chan++)
        d_active[i_chan] = d_scenario & (0x1 << i_chan);
}

int
dsa_pu_scenario_impl::work(int noutput_items,
                           gr_vector_const_void_star &input_items,
                           gr_vector_void_star &output_items)
{
    int n_chan = output_items.size();
    int n_out = 0;

    update_scenario(n_chan);
    
    assert(noutput_items >= d_pkt_len);

    // output 1 packet per active channel
    if (d_msg_queue.count() >= n_chan)
    {

        for(int i_chan = 0; i_chan < n_chan; i_chan++) 
        {
            gr_complex *out = (gr_complex *)output_items[i_chan];
            if (d_active[i_chan])
            {
                size_t io(0);
                pmt::pmt_t vect = pmt::cdr(d_msg_queue.delete_head());
                const uint8_t* ptr = (uint8_t *)uniform_vector_elements(vect, io);
                memcpy(out, ptr, d_pkt_len * sizeof(gr_complex));
                d_pkt_req--;
            }
            else // inactive channel
            {
                memset(out, 0, d_pkt_len * sizeof(gr_complex));
            }
        }
        n_out = d_pkt_len;
        d_pkt_cnt--;
    }

    // request more packets if our queue is low
    int num_request = d_num_queued - d_pkt_req;
    if (num_request > 0)
    {
        for (int i = 0; i < num_request; i++)
        {
            message_port_pub(pmt::mp("cmd"),
                             pmt::cons(pmt::mp("get_pkt"), pmt::mp(num_request)));
        }
        d_pkt_req += num_request;
    }

    // Tell runtime system how many output items we produced.
    return n_out;
}

} /* namespace cdc */
} /* namespace gr */

