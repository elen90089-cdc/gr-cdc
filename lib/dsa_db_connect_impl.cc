/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "dsa_db_connect_impl.h"

namespace gr {
namespace cdc {

dsa_db_connect::sptr
dsa_db_connect::make(std::string host,
                     int port,
                     int pktsize,
                     dsa::radio_type radio)
{
    return gnuradio::get_initial_sptr(
        new dsa_db_connect_impl(host, port, pktsize, radio));
}

/*
 * The private constructor
 */
dsa_db_connect_impl::dsa_db_connect_impl(std::string host,
                                         int port,
                                         int pktsize,
                                         dsa::radio_type radio)
    : gr::sync_block("dsa_db_connect",
          gr::io_signature::make(0, 0, 0),
          gr::io_signature::make(0, 0, 0)),
      d_pktsize(pktsize),
      d_iface(1500)
{
    message_port_register_in(pmt::mp("cmd"));
    message_port_register_out(pmt::mp("info"));
    message_port_register_out(pmt::mp("pkt"));

    set_msg_handler(pmt::mp("cmd"),
                    boost::bind(&dsa_db_connect_impl::handle_cmd, this, _1));

    d_iface.connect(host, port, radio);
}

/*
 * Our virtual destructor.
 */
dsa_db_connect_impl::~dsa_db_connect_impl()
{
    d_iface.disconnect();
}

void
dsa_db_connect_impl::handle_cmd(pmt::pmt_t msg)
{
    assert(pmt::pmt::is_pair(msg));
    std::string cmd = pmt::symbol_to_string(pmt::car(msg));
    pmt::pmt_t cdr = pmt::cdr(msg);

    if (cmd == "get_pkt") 
    {
        int pktcnt = pmt::to_long(cdr);
        for (int i = 0; i < pktcnt; i++)
        {
            pmt::pmt_t pdu = d_iface.get_packet(d_pktsize);
            message_port_pub(pmt::mp("pkt"), pdu);
        }
    }
    else if (cmd == "put_pkt")
    {
        d_iface.put_packet(cdr); 
    }
    else if (cmd == "get_kpi")
    {
        dsa_kpi_report report = d_iface.get_kpi_report();

        pmt::pmt_t kpi = pmt::make_dict();
        kpi = pmt::dict_add(kpi,
                            pmt::mp("pu_thruput_kbps"),
                            pmt::mp(report.pu_thruput_kbps));
        kpi = pmt::dict_add(kpi,
                            pmt::mp("pu_offered_kbps"),
                            pmt::mp(report.pu_offered_kbps));
        kpi = pmt::dict_add(kpi,
                            pmt::mp("su_thruput_kbps"),
                            pmt::mp(report.su_thruput_kbps));
        kpi = pmt::dict_add(kpi,
                            pmt::mp("su_offered_kbps"),
                            pmt::mp(report.su_offered_kbps));

        message_port_pub(pmt::mp("info"), pmt::cons(pmt::mp("kpi"), kpi));
    }
    else if (cmd == "get_mode")
    {
        int mode = d_iface.get_mode();
        message_port_pub(pmt::mp("info"),
                         pmt::cons(pmt::mp("mode"), pmt::mp(mode)));
    } 
    else if (cmd == "put_mode")
    {
        d_iface.put_mode(pmt::to_long(cdr));
    }
    else
    {
        // invalid command
    }
}

int
dsa_db_connect_impl::work(int noutput_items,
                          gr_vector_const_void_star &input_items,
                          gr_vector_void_star &output_items)
{
    return 0;
}

} /* namespace cdc */
} /* namespace gr */

