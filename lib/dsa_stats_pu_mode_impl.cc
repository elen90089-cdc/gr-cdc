/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "dsa_stats_pu_mode_impl.h"

namespace gr {
namespace cdc {

dsa_stats_pu_mode::sptr
dsa_stats_pu_mode::make(long period)
{
    return gnuradio::get_initial_sptr
        (new dsa_stats_pu_mode_impl(period));
}


/*
 * The private constructor
 */
dsa_stats_pu_mode_impl::dsa_stats_pu_mode_impl(long period)
    : gr::block("dsa_stats_pu_mode",
          gr::io_signature::make(0, 0, 0),
          gr::io_signature::make(0, 0, 0)),
      d_period(period)
{
    message_port_register_in(pmt::mp("pu"));
    message_port_register_in(pmt::mp("su"));
    message_port_register_out(pmt::mp("detect"));

    set_msg_handler(pmt::mp("pu"),
                    boost::bind(&dsa_stats_pu_mode_impl::handle_pu, this, _1));
    set_msg_handler(pmt::mp("su"),
                    boost::bind(&dsa_stats_pu_mode_impl::handle_su, this, _1));

    reset_stats();
}

/*
 * Our virtual destructor.
 */
dsa_stats_pu_mode_impl::~dsa_stats_pu_mode_impl()
{ }

bool
dsa_stats_pu_mode_impl::start()
{
    d_finished = false;

    d_thread = boost::shared_ptr<gr::thread::thread>(
        new gr::thread::thread(boost::bind(&dsa_stats_pu_mode_impl::run, this)));

    return block::start();
}

bool
dsa_stats_pu_mode_impl::stop()
{
    // Shut down the thread
    d_finished = true;
    d_thread->interrupt();
    d_thread->join();

    return block::stop();
}

void
dsa_stats_pu_mode_impl::run()
{
    while (!d_finished) {
        boost::this_thread::sleep(
            boost::posix_time::seconds(static_cast<long>(d_period)));
        
        if (d_finished)
            return;

        int num_mode;
        int num_detect;
        {
            gr::thread::scoped_lock(d_mtx);
            
            num_mode = d_num_mode;
            num_detect = d_num_detect;
        }

        double detect = 0.0;
        if (num_mode > 0)
            detect = (100.0 * num_detect) / num_mode;
        message_port_pub(pmt::mp("detect"), pmt::mp(detect));
   }
}

void
dsa_stats_pu_mode_impl::handle_pu(pmt::pmt_t msg)
{
    gr::thread::scoped_lock(d_mtx);

    if (d_num_correct > d_num_wrong)
        d_num_detect += 1;
    d_num_mode += 1;
    d_num_correct = 0;
    d_num_wrong = 0;

    d_mode = pmt::to_long(msg);
}

void
dsa_stats_pu_mode_impl::handle_su(pmt::pmt_t msg)
{
    gr::thread::scoped_lock(d_mtx);

    long mode = pmt::to_long(msg);
    if (mode == d_mode)
        d_num_correct += 1;
    else
        d_num_wrong += 1;
}

void
dsa_stats_pu_mode_impl::reset_stats(bool reset)
{
    if (reset)
    {
        gr::thread::scoped_lock(d_mtx);

        d_mode = -1;
        d_num_mode = -1;
        d_num_detect = 0;
        d_num_correct = 0;
        d_num_wrong = 0;
    }
}

} /* namespace cdc */
} /* namespace gr */

