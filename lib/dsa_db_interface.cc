/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include <cdc/dsa_db_interface.h>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

namespace gr {
namespace cdc {

dsa_db_interface::sptr dsa_db_interface::make(int mtu)
{
    return dsa_db_interface::sptr(new dsa_db_interface(mtu));
}


dsa_db_interface::dsa_db_interface(int mtu)
    : d_buf(mtu)
{ }

dsa_db_interface::~dsa_db_interface() { }

void
dsa_db_interface::connect(std::string host,
                          int port,
                          dsa::radio_type radio)
{
    tcp::endpoint endpoint(tcp::v4(), port);
    boost::system::error_code ec;

    d_socket.reset(new tcp::socket(d_io_context));
    d_socket->connect(endpoint, ec);
    d_socket->set_option(tcp::no_delay(true));

    if (ec)
        throw boost::system::system_error(ec);

    // send connect message
    d_buf[0] = static_cast<char>(dsa::REQUEST_CONNECT);
    d_buf[1] = static_cast<char>(radio);
    boost::asio::write(*d_socket, boost::asio::buffer(d_buf.data(), 2));

    d_radio = radio;
}

void
dsa_db_interface::disconnect()
{
    if (d_socket->is_open())
    {
        d_socket->close();
    }
}

pmt::pmt_t
dsa_db_interface::get_packet(int nbytes)
{
    d_buf[0] = static_cast<char>(dsa::REQUEST_GET_PKT);
    int n = htonl(nbytes);
    memcpy(d_buf.data() + 1, &n, sizeof(int));
    boost::asio::write(*d_socket, boost::asio::buffer(d_buf.data(), 5));

    boost::asio::read(*d_socket, boost::asio::buffer(d_buf.data(), nbytes));
    pmt::pmt_t payload = pmt::make_blob(d_buf.data(), nbytes);

    return pmt::cons(pmt::PMT_NIL, payload);
}

void
dsa_db_interface::put_packet(pmt::pmt_t& packet)
{
    d_buf[0] = static_cast<char>(dsa::REQUEST_PUT_PKT);

    int len = pmt::blob_length(packet);
    assert(len < d_buf.size() - 1);

    int nbytes = htonl(len);
    memcpy(d_buf.data() + 1, &nbytes, sizeof(int));

    size_t io(0);
    const uint8_t* ptr = (const uint8_t*)uniform_vector_elements(packet, io);
    memcpy(d_buf.data() + 5, ptr, len);

    boost::asio::write(*d_socket, boost::asio::buffer(d_buf.data(), len + 5));
}

dsa_kpi_report
dsa_db_interface::get_kpi_report(void)
{
    d_buf[0] = static_cast<char>(dsa::REQUEST_GET_KPI);
    boost::asio::write(*d_socket, boost::asio::buffer(d_buf.data(), 1));

    boost::asio::read(*d_socket, boost::asio::buffer(d_buf.data(), sizeof(dsa_kpi_report)));

    dsa_kpi_report report;
    memcpy(&report, d_buf.data(), sizeof(dsa_kpi_report));
    report.pu_thruput_kbps = ntohl(report.pu_thruput_kbps);
    report.pu_offered_kbps = ntohl(report.pu_offered_kbps);
    report.su_thruput_kbps = ntohl(report.su_thruput_kbps);
    report.su_offered_kbps = ntohl(report.su_offered_kbps);

    return report;
}

int
dsa_db_interface::get_mode(void)
{
    d_buf[0] = static_cast<char>(dsa::REQUEST_GET_MODE);
    boost::asio::write(*d_socket, boost::asio::buffer(d_buf.data(), 1));

    int mode;
    boost::asio::read(*d_socket, boost::asio::buffer(d_buf.data(), 4));
    memcpy(&mode, d_buf.data(), sizeof(4));
        
    return ntohl(mode);
}

void
dsa_db_interface::put_mode(int mode)
{
    d_buf[0] = static_cast<char>(dsa::REQUEST_PUT_MODE);
    mode = htonl(mode);
    memcpy(d_buf.data() + 1, &mode, sizeof(4));
    boost::asio::write(*d_socket, boost::asio::buffer(d_buf.data(), 5));
}

} /* namespace cdc */
} /* namespace gr */

