/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/io_signature.h>
#include "dsa_pu_scenario_2_impl.h"

namespace gr {
namespace cdc {

dsa_pu_scenario_2::sptr
dsa_pu_scenario_2::make(int scenario,
                        bool random,
                        int seed,
                        int samp_rate,
                        float duration_ms)
{
    return gnuradio::get_initial_sptr (new dsa_pu_scenario_2_impl(
        scenario, random, seed, samp_rate, duration_ms));
}

/*
 * The private constructor
 */
dsa_pu_scenario_2_impl::dsa_pu_scenario_2_impl(int scenario,
                                               bool random,
                                               int seed,
                                               int samp_rate,
                                               float duration_ms)
    : gr::sync_block("dsa_pu_scenario_2",
          gr::io_signature::make(1, -1, sizeof(gr_complex)),
          gr::io_signature::make(1, -1, sizeof(gr_complex))),
      d_scenario(scenario),
      d_random(random),
      d_engine(seed),
      d_samp_rate(samp_rate),
      d_duration_ms(duration_ms)
{
    message_port_register_out(pmt::mp("mode"));
}

/*
 * Our virtual destructor.
 */
dsa_pu_scenario_2_impl::~dsa_pu_scenario_2_impl()
{
    if (d_uniform)
        delete d_uniform;
}

void
dsa_pu_scenario_2_impl::update_scenario(int n_chan)
{
    if (!d_active.size())
    {
        d_active.resize(n_chan);
        int n_scenarios = (0x1 << n_chan);
        d_uniform = new std::uniform_int_distribution<int>(0, n_scenarios - 1);
    }

    if (d_samp_cnt <= 0)
    {
        d_samp_cnt = int(d_duration_ms * d_samp_rate / 1000.0);
        if (d_random)
            d_scenario = (*d_uniform)(d_engine);
        message_port_pub(pmt::mp("mode"), pmt::mp(d_scenario));
    }

    for (int i_chan=0; i_chan < n_chan; i_chan++)
        d_active[i_chan] = d_scenario & (0x1 << i_chan);
}

int
dsa_pu_scenario_2_impl::work(int noutput_items,
                             gr_vector_const_void_star &input_items,
                             gr_vector_void_star &output_items)
{
    int n_chan = output_items.size();

    update_scenario(n_chan);

    int n_samps = (d_samp_cnt < noutput_items) ? d_samp_cnt : noutput_items;
    for (int i_chan = 0; i_chan < n_chan; i_chan++)
    {
        gr_complex *in = (gr_complex *)input_items[i_chan];
        gr_complex *out = (gr_complex *)output_items[i_chan];
        if (d_active[i_chan])
            memcpy(out, in, n_samps * sizeof(gr_complex));
        else
            memset(out, 0x0, n_samps * sizeof(gr_complex));
    }
    d_samp_cnt -= n_samps;

    return n_samps;
}

} /* namespace cdc */
} /* namespace gr */

