/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_PU_SCENARIO_H
#define INCLUDED_CDC_DSA_PU_SCENARIO_H

#include <cdc/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace cdc {

/*!
 * \brief Implements primary user scenario control for CDC dynamic spectrum
          access project.
 * \ingroup cdc
 *
 */
class CDC_API dsa_pu_scenario : virtual public gr::sync_block
{
public:
    typedef boost::shared_ptr<dsa_pu_scenario> sptr;

    /*!
    * \brief DSA PU Scenario Constructor
    *
    * \param scenario       Primary user scenario - bitmap of active channels,
    *                       e.g., for 4 PU channels SCENARIO = 10 = 0b1010
                            indicates channels 2 and 4 are active. Set to -1
    *                       for randomly selected scenarios.
    * \param num_packets    Number of packets to send before updating random
                            scenario.
    * \param num_queued     Number of packets to queue before beginning
    *                       transmission. 
    * \param seed           Seed used used in random scenario selection.
    */
    static sptr make(int scenario,
                     int num_packets,
                     int num_queued,
                     int seed);
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_PU_SCENARIO_H */

