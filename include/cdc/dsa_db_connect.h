/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_DB_CONNECT_H
#define INCLUDED_CDC_DSA_DB_CONNECT_H

#include <cdc/api.h>
#include <cdc/dsa.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace cdc {

/*!
 * \brief Connect to DSA database server.
 * \ingroup cdc
 */
class CDC_API dsa_db_connect : virtual public gr::sync_block
{
public:
    typedef boost::shared_ptr<dsa_db_connect> sptr;

    /*!
    * \brief DSA DB Connect Constructor
    *
    * \param host       The name or IP address of DSA database server
    * \param port       Destination port to connect to on server
    * \param pktsize    Size of requested packets (in bytes)
    * \param radio      DSA radio type (Primary Tx, Primary Rx, ...)
    */
    static sptr make(std::string host,
                     int port,
                     int pktsize,
                     dsa::radio_type radio);
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_DB_CONNECT_H */

