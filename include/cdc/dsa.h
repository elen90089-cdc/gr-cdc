/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_H
#define INCLUDED_CDC_DSA_H

#include <cdc/api.h>

namespace gr {
namespace cdc {

/*!
 * \brief 
 */
class CDC_API dsa_kpi_report
{
public:        
    int pu_thruput_kbps = 0; //!< primary user throughput (kbps)
    int pu_offered_kbps = 0; //!< primary user offered throughput (kbps)
    int su_thruput_kbps = 0; //!< secondary user throughput (kbps)
    int su_offered_kbps = 0; //!< secondary user offered throughput (kbps)
};

class CDC_API dsa
{
public:
    enum radio_type
    {
        RADIO_PU_TX = 0, //!< primary user transmitter
        RADIO_PU_RX = 1, //!< primary user receiver
        RADIO_SU_TX = 2, //!< secondary user transmitter
        RADIO_SU_RX = 3, //!< secondary user receiver
    };

    enum request_type
    {
        REQUEST_CONNECT = 1,   //!< database connect request
        REQUEST_GET_PKT = 2,   //!< get new packet request
        REQUEST_PUT_PKT = 3,   //!< report correctly received packet request
        REQUEST_GET_KPI = 4,   //!< get current KPI report request
        REQUEST_GET_MODE = 5,  //!< get current primary user mode request
        REQUEST_PUT_MODE = 6,  //!< report current primary user mode request
        REQUEST_RESET_KPI = 7, //!< reset statistics request
    };

};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_H */

