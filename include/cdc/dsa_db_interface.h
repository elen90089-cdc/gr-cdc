/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_DB_INTERFACE_H
#define INCLUDED_CDC_DSA_DB_INTERFACE_H

#include <cdc/api.h>
#include <cdc/dsa.h>
#include <gnuradio/block.h>
#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>


namespace gr {
namespace cdc {

/*!
 * \brief Interface to remote DSA database.
 * \ingroup cdc
 */
class CDC_API dsa_db_interface
    : public boost::enable_shared_from_this<gr::cdc::dsa_db_interface> 
{
public:
    typedef boost::shared_ptr<dsa_db_interface> sptr;

    dsa_db_interface(int mtu = 1500);
    ~dsa_db_interface();

    sptr base() { return shared_from_this(); };

    /*!
     * \brief Connect to DSA database
     *
     * \param host      Name or IP address of DSA database server
     * \param port      Destination port number of database
     * \param radio     DSA radio type (primary tx, primary rx, ...)
     */
    void connect(std::string host,
                 int port,
                 dsa::radio_type radio);

    //! Disconnect from database server
    void disconnect();

    //! Get a new data packet of size NBYTES from database
    pmt::pmt_t get_packet(int nbytes);

    //! Report a correctly received data packet to database
    void put_packet(pmt::pmt_t& packet);

    //! Get current statistics as KPI report from database
    dsa_kpi_report get_kpi_report(void);
        
    //! Get current primary user mode from database (development only)
    int get_mode(void);

    //! Update primary user mode in remote database
    void put_mode(int mode);

    /*!
     * \brief DSA DB Interface Constructor
     *
     * \param mtu       Maximum transmission unit (in bytes)
     */
    static sptr make(int mtu = 1500);

private:
    dsa::radio_type d_radio;
    std::vector<char> d_buf;
    boost::asio::io_context d_io_context;
    std::unique_ptr<boost::asio::ip::tcp::socket> d_socket;
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_DB_INTERFACE_H */
