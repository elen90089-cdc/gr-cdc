/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_STATS_PU_MODE_H
#define INCLUDED_CDC_DSA_STATS_PU_MODE_H

#include <cdc/api.h>
#include <gnuradio/block.h>

namespace gr {
namespace cdc {

/*!
 * \brief <+description of block+>
 * \ingroup cdc
 *
 */
class CDC_API dsa_stats_pu_mode : virtual public gr::block
{
public:
    typedef boost::shared_ptr<dsa_stats_pu_mode> sptr;

    virtual void reset_stats(bool reset) = 0;

    /*!
     * \brief Return a shared_ptr to a new instance of cdc::dsa_stats_pu_mode.
     *
     * To avoid accidental use of raw pointers, cdc::dsa_stats_pu_mode's
     * constructor is in a private implementation
     * class. cdc::dsa_stats_pu_mode::make is the public interface for
     * creating new instances.
     */
    static sptr make(long period);
};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_STATS_PU_MODE_H */

