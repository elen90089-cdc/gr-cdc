/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_CDC_DSA_DATABASE_H
#define INCLUDED_CDC_DSA_DATABASE_H

#include <cdc/api.h>
#include <cdc/dsa.h>
#include <chrono>
#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <gnuradio/thread/thread.h>

namespace gr {
namespace cdc {

/*!
 * \brief Monitor statistics of dynamic spectrum access scenario.
 * \ingroup cdc
 *
 * \details
 * Implements a central database to monitor the statistics of 
 * primary and secondary links in CDC dynamic spectrum access scenario.
 */
class CDC_API dsa_database
    : public boost::enable_shared_from_this<gr::cdc::dsa_database>
{
public:
    typedef boost::shared_ptr<dsa_database> sptr;

    dsa_database(std::string host,
                 int port,
                 bool share_mode,
                 int mtu = 1500);
    ~dsa_database();

    sptr base() { return shared_from_this(); };

    //! Start accepting connections and tracking statistics
    void start();

    //! Stop accepting connections and tracking statistics
    void stop();

    //! Return current link statistics as KPI report
    dsa_kpi_report get_kpi_report();

    //! Reset link statistics
    void reset_stats();

    //! Returns reported primary user operation mode
    int get_pu_mode();

    //! Updates primary user operation mode
    void set_pu_mode(int pu_mode);

    /*!
     * \brief DSA Database Constructor
     *
     * \param host          Name or IP address of interface to bind to
     * \param port          Port number on which to accept incoming connections
     * \param share_mode    Answer primary user mode requests from secondary link
                            connections (development mode)
     * \param mtu           Maximum transmission unit (in bytes)
     */
    static sptr make(std::string host,
                     int port,
                     bool share_mode,
                     int mtu = 1500);

private:
    std::string d_host;
    int d_port;
    bool d_share_mode;
    int d_mtu;
    
    boost::asio::io_context d_io_context;
    gr::thread::thread d_listener;
    gr::thread::mutex d_mtx_thr;
    std::vector<bool> d_radios;
    bool d_stop = true;

    // statistics
    gr::thread::mutex d_mtx_stats;
    std::chrono::time_point<std::chrono::steady_clock> d_timepoint;
    int d_pu_mode = -1;
    double d_pu_bytes = 0;
    double d_pu_offer = 0;
    double d_su_bytes = 0;
    double d_su_offer = 0;

    void accept_handler();

    void request_handler(std::shared_ptr<boost::asio::ip::tcp::socket> sock,
                         dsa::radio_type radio);

    void update_stats(dsa::radio_type radio,
                      int nbytes);

};

} // namespace cdc
} // namespace gr

#endif /* INCLUDED_CDC_DSA_DATABASE_H */

