#
# Copyright 2021 University of Melbourne.
#

import numpy as np
from gnuradio import gr
import pmt

class fixed_pdu_generator(gr.sync_block):
    """
    This block sends a PDU with a specified payload whenever a message is
    received on its input message port.
    """
    def __init__(self, payload=[]):
        gr.sync_block.__init__(
            self,
            name='fixed_pdu_generator',
            in_sig=None,
            out_sig=None,
        )
        self.message_port_register_in(pmt.intern('generate'))
        self.message_port_register_out(pmt.intern('pdu'))
        self.set_msg_handler(pmt.intern('generate'), self.handle_msg)
        self.payload = payload

    def handle_msg(self, msg):
        pdu = pmt.cons(
            pmt.PMT_NIL, pmt.init_u8vector(len(self.payload), self.payload))
        self.message_port_pub(pmt.intern('pdu'), pdu)
