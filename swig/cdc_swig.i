/* -*- c++ -*- */

#define CDC_API

%include "gnuradio.i"           // the common stuff

//load generated python docstrings
%include "cdc_swig_doc.i"

%{
#include "cdc/dsa.h"
#include "cdc/dsa_pu_scenario.h"
#include "cdc/dsa_pu_scenario_2.h"
#include "cdc/dsa_db_connect.h"
#include "cdc/dsa_database.h"
#include "cdc/dsa_db_interface.h"
#include "cdc/dsa_stats_thruput.h"
#include "cdc/dsa_stats_pu_mode.h"
%}

%include "cdc/dsa.h"
%include "cdc/dsa_pu_scenario.h"
GR_SWIG_BLOCK_MAGIC2(cdc, dsa_pu_scenario);
%include "cdc/dsa_pu_scenario_2.h"
GR_SWIG_BLOCK_MAGIC2(cdc, dsa_pu_scenario_2);
%include "cdc/dsa_db_connect.h"
GR_SWIG_BLOCK_MAGIC2(cdc, dsa_db_connect);
%include "cdc/dsa_database.h"
%include "cdc/dsa_db_interface.h"
%include "cdc/dsa_stats_thruput.h"
GR_SWIG_BLOCK_MAGIC2(cdc, dsa_stats_thruput);
%include "cdc/dsa_stats_pu_mode.h"
GR_SWIG_BLOCK_MAGIC2(cdc, dsa_stats_pu_mode);


// properly package up non-block objects
%include "dsa.i"
