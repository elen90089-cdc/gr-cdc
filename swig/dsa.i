/* -*- c++ -*- */
/*
 * Copyright 2021 University of Melbourne.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

%template(dsa_database_sptr) boost::shared_ptr<gr::cdc::dsa_database>;
%pythoncode %{
dsa_database_sptr.__repr__ = lambda self: "<dsa_database>"
dsa_database = dsa_database .make;
%}

%template(dsa_db_interface_sptr) boost::shared_ptr<gr::cdc::dsa_db_interface>;
%pythoncode %{
dsa_db_interface_sptr.__repr__ = lambda self: "<dsa_db_interface>"
dsa_db_interface = dsa_db_interface .make;
%}
